<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prestamo extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            if ($this->perfilid==1) {
                $this->sucursalId=0;
            }
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,22);// 1 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
    }
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('prestamo/listado');
        $this->load->view('templates/footer');
        $this->load->view('prestamo/listadojs');     
	}
    function searchempleado($id){
        $search = $this->input->get('search');
        $where =  array('activo' =>1,'sucursalId'=>$id);
        $results=$this->ModeloCatalogos->getselectwherelike1('personal',$where,'nombre',$search);
        echo json_encode($results);
    }
    public function addprestamo($id=0){
        $where=array('activo'=>1);
        $data['sucursales']=$this->ModeloCatalogos->getselectvalue1rowwheren('sucursales',$where);
        $data['m']=$this->fechahoy;
        if($id==0){
            $data['idprestamo']=0;
            $data['sucursalId']=0;
            $data['personalId']=0;
            $data['empleado']='';
            $data['concepto']='';
            $data['monto']='';
            $data['fecha']=0;

        }else{
            $result=$this->ModeloCatalogos->getselectvalue1rowwhere('prestamo','idprestamo',$id);
            foreach ($result->result() as $r) {
                $data['idprestamo']=$r->idprestamo;
                $data['personalId']=$r->personalId;
                $idp=$r->personalId;
                $data['concepto']=$r->concepto;
                $data['monto']=$r->monto;
                $data['fecha']=$r->fecha;
                $resultp=$this->ModeloCatalogos->getselectvalue1rowwhere('personal','personalId',$idp);
                foreach ($resultp->result() as $r){
                    $data['empleado']=$r->nombre;
                    $data['sucursalId']=$r->sucursalId;
                }    
                
            }
        }
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('prestamo/prestamo',$data);
        $this->load->view('templates/footer');
        $this->load->view('prestamo/prestamojs');     
    }
    function addregistro(){
        $data = $this->input->post();
        $total_pres=0;
        $id=$data['idprestamo'];
        $personalId=$data['personalId'];
        unset($data['idprestamo']); 
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogo('prestamo',$data,'idprestamo',$id);
        }else{
            $this->ModeloCatalogos->Insert('prestamo',$data);
        }
        $where_prest = array('personalId'=>$personalId,'status'=>1,'activo'=>1);
        $result_pr=$this->ModeloCatalogos->getselectvalue1rowwheren('prestamo',$where_prest);
        $monto_total=0;
        foreach ($result_pr->result() as $pr){
            $monto_total=$monto_total+$pr->monto;
        }
        $data_p = array('prestamo'=>$monto_total);
        $this->ModeloCatalogos->updateCatalogo('personal',$data_p,'personalId',$personalId);
    }
    function getData(){
        $params = $this->input->post();
        $tablelistado = $this->ModeloGeneral->getlistpretsamo($params);
        $tablelistadorow=$this->ModeloGeneral->total_prestamo($params);
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($tablelistadorow),  
            "recordsFiltered" => intval($tablelistadorow),
            "data"            => $tablelistado->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function getprestamo(){
        $params = $this->input->post();
        $tablelistado = $this->ModeloGeneral->getlistpretsamo_detalles($params);
        $tablelistadorow=$this->ModeloGeneral->total_prestamo_detalles($params);
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($tablelistadorow),  
            "recordsFiltered" => intval($tablelistadorow),
            "data"            => $tablelistado->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function eliminar(){
        $id = $this->input->post('idp');
        ///
        $id_prest = array('idprestamo'=>$id);
        $result_pr_per=$this->ModeloCatalogos->getselectvalue1rowwheren('prestamo',$id_prest);
        foreach ($result_pr_per->result() as $pr){
            $personalId_e=$pr->personalId;
        }
        /// Eliminar
        $data = array('activo' => 0);
        $this->ModeloCatalogos->updateCatalogo('prestamo',$data,'idprestamo',$id);
        /// Actulizar
        $where_prest = array('personalId'=>$personalId_e,'status'=>1,'activo'=>1);
        $result_pr=$this->ModeloCatalogos->getselectvalue1rowwheren('prestamo',$where_prest);
        $monto_total=0;
        foreach ($result_pr->result() as $pr){
            $monto_total=$monto_total+$pr->monto;
        }
        ///
        $data_p = array('prestamo'=>$monto_total);
        $this->ModeloCatalogos->updateCatalogo('personal',$data_p,'personalId',$personalId_e);
        ///
    }
}