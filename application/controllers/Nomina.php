<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nomina extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            if ($this->perfilid==1) {
                $this->sucursalId=0;
            }
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,23);// 1 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
    }
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('nomina/nomina');
        $this->load->view('templates/footer');
        $this->load->view('nomina/nominajs');     
	}
    public function registrar(){
        $data['fechahoy']=$this->fechahoy;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('nomina/nomina',$data);
        $this->load->view('templates/footer');
        $this->load->view('nomina/nominajs');
    }
    function getData(){
        $params = $this->input->post();
        $tablelistado = $this->ModeloGeneral->getlistnomina($params);
        $tablelistadorow=$this->ModeloGeneral->total_nomina($params);
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($tablelistadorow),  
            "recordsFiltered" => intval($tablelistadorow),
            "data"            => $tablelistado->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function get_prestamo(){
        $id = $this->input->post('id');
        $fechai = $this->input->post('fechai');
        $fechaf = $this->input->post('fechaf');
        echo $result=$this->ModeloGeneral->get_pestamo($id,$fechai,$fechaf);
    }
    public function registra_nomina(){
        $id = $this->input->post('id');
        $fecha = $this->input->post('fecha');
        $sueldo = $this->input->post('sueldo');
        $prestamo = $this->input->post('prestamo');
        $total_s = $this->input->post('total_s');
        $data = array('personalId'=>$id,
                      'sueldo'=>$sueldo,
                      'descuento_prestamo'=>$prestamo,
                      'fecha'=>$fecha,
                      'total'=>$total_s
                      ); 
        $this->ModeloCatalogos->Insert('nomina',$data);
        $info = array('status'=>0);
        $where = array('personalId' =>$id,'activo'=>1);
        $this->ModeloCatalogos->update_tabla('prestamo',$info,$where);
        $info_p = array('prestamo'=>0);
        $where_p = array('personalId' =>$id);
        $this->ModeloCatalogos->update_tabla('personal',$info_p,$where_p);

    }
    public function get_validar_nomina(){
        $id = $this->input->post('id');
        $fecha = $this->input->post('fecha');
        $aux_p=0;
        $prestamo=0;
        $tt_pagar=0;
        $resul=$this->ModeloGeneral->get_validar_nomina($id,$fecha);
        foreach ($resul->result() as $item){
            $aux_p=1;
            $prestamo=$item->descuento_prestamo;
            $tt_pagar=$item->total;
        }
        $arrayinfo = array('aux_p'=>$aux_p,'prestamo'=>$prestamo,'tt_pagar'=>$tt_pagar);
        echo json_encode($arrayinfo);
    }
    /*
    public function get_validar_nomina(){
        $id = $this->input->post('id');
        $fechai = $this->input->post('fecha');
        $resul=$this->ModeloGeneral->get_validar_nomina($id,$fecha);
        echo $resul;
    }
    */
}