<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cajas extends CI_Controller {
	public function __construct()
    {
      parent::__construct();
      $this->load->helper('url');
      $this->load->model('ModeloCatalogos');
      $this->load->model('ModeloGeneral');
      if (!$this->session->userdata('logeado')){
          redirect('/Login');
      }else{
          $this->perfilid=$this->session->userdata('perfilid');
          //ira el permiso del modulo
          $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,24);// 6 es el id del submenu
          if ($permiso==0) {
              redirect('/Sistema');
          }
      }
      date_default_timezone_set('America/Mexico_City');
      $this->fechahoy = date('Y-m-d G:i:s');
    }
    public function index(){
      $this->load->view('templates/header');
      $this->load->view('templates/navbar');
      $this->load->view('cajas/listcajas');
      $this->load->view('templates/footer');
      $this->load->view('cajas/listcajasjs');
    }
    public function getData_listado(){
        $params = $this->input->post();
        $productos = $this->ModeloGeneral->getlistcajas($params);
        $totalRecords=$this->ModeloGeneral->total_cajas($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function add(){
      $idc = $this->input->post('idc');
      $cajas = $this->input->post('cajas');
      $info = array('ClientesId'=>$idc,'cajas'=>$cajas,'reg'=>$this->fechahoy);
      $this->ModeloCatalogos->Insert('devolucioncajas',$info);
      $where = array('ClientesId' => $idc);
      $result_cli=$this->ModeloCatalogos->getselectvalue1rowwheren('clientes',$where);
      foreach ($result_cli->result() as $e){
        $cajas_c=$e->cajas;
      }
      $resta=$cajas_c-$cajas;
      $data = array('cajas' => $resta);
      $this->ModeloCatalogos->update_tabla('clientes',$data,$where);
    }
}