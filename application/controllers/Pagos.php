<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagos extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,20);// 13 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
    }
     
    public function index(){
        $result=$this->ModeloCatalogos->getselectvalue1rowwhere('configuracion','id',1);
        foreach ($result->result() as $row) {
                $data['usuariosmaximo']=$row->usuariosmaximo;
                $data['productosmaximos']=$row->productosmaximos;
                $data['vigencia']=$row->vigencia;
                $data['costomensual']=$row->costomensual;
                $data['costoanual']=$row->costoanual;
            }
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('config/pagos',$data);
        $this->load->view('templates/footer');

    }




}