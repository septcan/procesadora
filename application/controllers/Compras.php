<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compras extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->personal=$this->session->userdata('idpersonal');
            $this->sucursalId=$this->session->userdata('sucursalId');
            $this->sucursalIdd=$this->sucursalId;
            if ($this->perfilid==1) {
                $this->sucursalId=0; 
            }
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,5);// 6 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
    }
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('compras/compraslista');
        $this->load->view('templates/footer');
        $this->load->view('compras/compraslistajs');
	}
    function Comprasadd(){
        $data['sucursalesrow']=$this->ModeloCatalogos->getselectvalue1rowwhere('sucursales','activo',1);
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('compras/compraadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('compras/compraaddjs');

    }
    function ingresarcompra(){
        $data = $this->input->post();
        $data['personalId']=$this->personal;
        $id_proveedor=$data['id_proveedor'];
        $data['reg']=$this->fechahoy;
        $taras=$data['cajas_total'];
        $data['montocredito']=$data['monto_total'];
        $id=$this->ModeloCatalogos->Insert('compras',$data);

        $where_prest_cajas = array('id_proveedor'=>$id_proveedor);
        $result_cajas=$this->ModeloCatalogos->getselectvalue1rowwheren('proveedores',$where_prest_cajas);
        $cajas_total=0;
        foreach ($result_cajas->result() as $pr){
            $cajas_total=$pr->cajas+$taras;
        }
        $data_p = array('cajas'=>$cajas_total);
        $this->ModeloCatalogos->updateCatalogo('proveedores',$data_p,'id_proveedor',$id_proveedor);
        echo $id;
    }
    function ingresarcomprapro(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $data['compraId'] = $DATA[$i]->compraId;
            $productoid=$DATA[$i]->productoid;
            $data['productoid'] = $productoid;
            $cantidad=$DATA[$i]->cantidad;
            $data['cantidad'] = $DATA[$i]->cantidad;
            $precio_compra=$DATA[$i]->precio_compra;
            $data['precio_compra'] = $precio_compra;
            $cajas=$DATA[$i]->precio_compra;
            $data['cajas'] = $cajas;
            $id=$this->ModeloCatalogos->Insert('compras_detalles',$data);

            $datapro = array('preciocompra'=>$precio_compra);
            $this->ModeloCatalogos->updateCatalogo('productos',$datapro,'productoid',$productoid);
            /*
            $stock='stock'.$this->sucursalIdd;
            $this->ModeloCatalogos->updatestock('productos',$stock,'+',$cantidad,'productoid',$productoid);
            */
            $this->ModeloCatalogos->updatestock2('productos_sucursales','existencia','+',$cantidad,'idproducto',$productoid,'idsucursal',$DATA[$i]->sucursalid);
        }
    }
    function compraproductos(){
        $id = $this->input->post('compraid');
        $result=$this->ModeloCatalogos->compraproductos($id);
        $html='<table class="table table-striped jambo_table bulk_action" id="tableproductoscompras">
                <thead>
                    <tr>
                        <th>Cantidad</th>
                        <th>Producto</th>
                        <th>Precio Compra</th>
                    </tr>
                <thead>
                <tbody>
                ';
        foreach ($result->result() as $item) {
            $html.='<tr>
                        <td>'.$item->cantidad.'</td>
                        <td>'.$item->nombre.'</td>
                        <td>'.$item->precio_compra.'</td>
                    </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    public function getData_listado(){
        $params = $this->input->post();
        $productos = $this->ModeloCatalogos->List_table_compras($params,$this->sucursalId);
        $totalRecords=$this->ModeloCatalogos->filastotal_compras($params,$this->sucursalId); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function pagos(){
        $num = $this->input->post('compId');
        $result = $this->ModeloCatalogos->List_table_pagos_compras($num);
        $html = '';
            $html .= '<table class="table table-striped jambo_table bulk_action" id="data-tables">';
            $html .= '<thead>';
              $html .= '<tr>';
                $html .= '<th>#</th>';
                $html .= '<th>Pago</th>';
                $html .= '<th>Fecha deposito</th>';
              $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            $suma=0;
              foreach ($result->result() as $item) {
                    $suma=$suma+$item->pago;
                    $html .="<tr>";
                      $html .= "<td>".$item->pagoId."</td>";
                      $html .= "<td>".$item->pago."</td>";
                      $html .= "<td>".$item->reg."</td>";
                    $html .= "</tr>";
                 }
            $html .= "</tbody>";
          $html .= "</table>";
          $html .= "<div class='row'>";
            $where_prest_cajas = array('compraId'=>$num);
            $result_c=$this->ModeloCatalogos->getselectvalue1rowwheren('compras',$where_prest_cajas);
            $total_compra=0;
            foreach ($result_c->result() as $pr){
                $total_compra=$pr->montocredito;
            }
              $html .= "<div class='col-md-4 col-sm-4 col-xs-12'>Total: <b>$".bcdiv($suma, '1', 1)."</b></div><div class='col-md-4 col-sm-4 col-xs-12'>Resta: <b>$".bcdiv($total_compra, '1', 1)."</b> </div>";
            $html .= "</div>";
            $html .= "<br>";
        $html .= '';

        echo $html;

    } 
    public function add(){
        $compraId = $this->input->post('comprId');
        $pago = $this->input->post('pago');

        $data['compraId'] = $compraId;
        $data['pago'] = $pago;
        $data['personalId'] = $this->personal;
        $data['reg'] = $this->fechahoy;

        $id=$this->ModeloCatalogos->Insert('pagos_credito_compras',$data);
        /////////////////////////////////////////////////// Agregar cajas al cliente
        $where_prest_cajas = array('compraId'=>$compraId);
        $result_c=$this->ModeloCatalogos->getselectvalue1rowwheren('compras',$where_prest_cajas);
        $total_compra=0;
        foreach ($result_c->result() as $pr){
            $total_compra=$pr->montocredito-$pago;
        }
        $data_p = array('montocredito'=>$total_compra);
        $this->ModeloCatalogos->updateCatalogo('compras',$data_p,'compraId',$compraId);
    echo $x;
    }

}