<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gastos extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->personal=$this->session->userdata('idpersonal');
            $this->sucursalId=$this->session->userdata('sucursalId');
            if ($this->perfilid==1) {
                $this->sucursalId=0;
                $this->sucursalIdd=$this->sucursalId;
            }else{
                $this->sucursalIdd=$this->sucursalId;
            }
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,15);// 15 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
    }
	public function index(){
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            //ira el permiso del modulo
        }
        //====================================
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('gastos/gastoslist');
        $this->load->view('templates/footer');
        $this->load->view('gastos/listgastosjs');
	}

    function Gastosadd(){
        $data['sucursalIdd']=$this->sucursalIdd;
        $data['perfilid']=$this->perfilid;
        $data['sucursal']=$this->ModeloCatalogos->getselectvalue1rowwhere('sucursales','activo',1);
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('gastos/gastosadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('gastos/gastoslistjs');
    }

    function cancelar(){
        $id = $this->input->post('idgasto');
        $motivo = $this->input->post('motivo_cancela');;
        $data = array('activo' => 0,'personalId_cancela' => $this->personal, 'motivo_cancela' => $motivo['motivo_cancela'],'reg_cancela'=>$this->fechahoy);
        $this->ModeloCatalogos->updateCatalogo('gastos',$data,' gastosid',$id);
    }
    function add(){
        $data = $this->input->post();
        $data['personalId'] = $this->personal;
        $this->ModeloCatalogos->Insert('gastos',$data);
    }
        public function getData_listado(){
        $params = $this->input->post();
        $productos = $this->ModeloCatalogos->List_table_gastos($params,$this->sucursalId);
        $totalRecords=$this->ModeloCatalogos->filastotal_gastos($params,$this->sucursalId); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
}    