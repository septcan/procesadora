
<style type="text/css">
	.iq-over-blue-90:before { content: ""; height: 100%; left: 0; position: absolute; top: 0; width: 100%; z-index: -1; background: rgba(36, 107, 173, 0.9); background: -moz-linear-gradient(-45deg, rgba(36, 107, 173, 0.9) 0%, rgba(0, 200, 200, 0.9) 100%); background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(36, 107, 173, 0.9)), color-stop(100%, rgba(0, 200, 200, 0.9))); background: -webkit-linear-gradient(-45deg, rgba(36, 107, 173, 0.9) 0%, rgba(0, 200, 200, 0.9) 100%); background: -o-linear-gradient(-45deg, rgba(36, 107, 173, 0.9) 0%, rgba(0, 200, 200, 0.9) 100%); background: -ms-linear-gradient(-45deg, rgba(36, 107, 173, 0.9) 0%, rgba(0, 200, 200, 0.9) 100%); background: linear-gradient(135deg, rgba(36, 107, 173, 0.9) 0%, rgba(0, 200, 200, 0.9) 100%); filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#246bad', endColorstr='#00c8c8', GradientType=1); }
</style>
<div class="limiter">
		<div class="container-login100 iq-over-blue-90">
			<div class="wrap-login100  zoomIn animated">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="<?php echo base_url(); ?>public/images/pos.svg" alt="IMG">
				</div>

				<form class="login100-form validate-form" id="login-form" action="#" role="form" autocomplete="off">
					<!--
					<span class="login100-form-title">
						Member Login
					</span>-->

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="txtUsuario" id="txtUsuario" placeholder="Usuario" autocomplete="off">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="txtPass" id="txtPass" placeholder="Password" autocomplete="new-password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" id="login-submit">
							Login
						</button>
					</div>
					<!--
					<div class="text-center p-t-12">
						<span class="txt1">
							Forgot
						</span>
						<a class="txt2" href="#">
							Username / Password?
						</a>
					</div>-->
				</form>
			</div>
		</div>
	</div>