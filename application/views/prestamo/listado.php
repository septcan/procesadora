<div class="right_col" role="main">
  <div class="col-md-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Prestamo </h2>

          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="row text-right">
            <a class="btn btn-dark float-right" href="<?php echo base_url() ?>Prestamo/addprestamo">Nuevo</a>
          </div>
          <table class="table table-striped jambo_table bulk_action" id="data_tables">
            <thead>
              <tr >
                <th>#</th>
                <th>Empleado</th>
                <th>Sucursal</th>
                <th>Monto</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>            
  </div>
</div>
 
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="modal_detalles">
  <div class="modal-dialog ">
    <div class="modal-content curba">
      <div class="modal-header curbaa foot">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h3 class="modal-title" id="myModalLabel2">Detalles de <span class="empleado_text"></span></h3>
      </div>
      <div class="modal-body">
      <div class="row">
        <div class="col-md-12">
          <div class="tabla_prestamos"></div>
        </div>
      </div>  
        
      </div>
      <div class="modal-footer curbab foot">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>    
<!-- /page content -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="eliminar_modal">
  <div class="modal-dialog">
    <div class="modal-content curba">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h3 class="modal-title" id="myModalLabel2">Confirmación</h3>
      </div>
      <div class="modal-body">
        <h4>¿Está seguro que desea eliminar este registros?</h4>
        <input type="hidden" id="idprestamo_e">
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="boton_eliminar()"><i class="fa fa-trash"></i> Eliminar</button>
      </div>

    </div>
  </div>
</div>  