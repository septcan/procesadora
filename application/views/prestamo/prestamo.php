<div class="right_col" role="main">
  <div class="col-md-12"> 
      <div class="x_panel">
        <div class="x_title">
          <h2>Prestamo </h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <label class="col-md-2 col-sm-2 col-xs-12 control-label">Sucursal:</label>
              <div class="col-md-10 col-sm-10 col-xs-12 controls">
                <select name="sucursalId" id="sucursalId" class="form-control">
                  <?php foreach ($sucursales->result() as $item) { ?>
                    <option value="<?php echo $item->sucursalid;?>" <?php if($item->sucursalid==$sucursalId) echo 'selected' ?>><?php echo $item->sucursal;?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <hr>
          <form method="post"  role="form" id="form_prestamo">
            <input type="hidden" name="idprestamo" value="<?php echo $idprestamo ?>">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div>
                <label class="col-md-2 col-sm-3 col-xs-12 control-label">Empledos:</label>
                <div class="col-md-10 col-sm-9 col-xs-12">
                    <select class="form-control" name="personalId" id="personalId">
                      <?php if($idprestamo!=0){
                       echo '<option value="'.$personalId.'">'.$empleado.'</option>';
                      }?>
                    </select>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-2 col-sm-3 col-xs-12 control-label">Concepto:</label>
                <div class="col-md-10 col-sm-9 col-xs-12 controls">
                  <input type="text" name="concepto" class="form-control has-feedback-left" value="<?php echo $concepto ?>">
                  <span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-2 col-sm-3 col-xs-12 control-label">Monto:</label>
                <div class="col-md-10 col-sm-9 col-xs-12 controls">
                  <input type="number" name="monto" class="form-control has-feedback-left" value="<?php echo $monto ?>">
                  <span class="fa fa-dollar form-control-feedback left" aria-hidden="true"></span>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="col-md-2 col-sm-3 col-xs-12 control-label">Fecha:</label>
                <div class="col-md-10 col-sm-9 col-xs-12 controls">
                  <input type="date" name="fecha" class="form-control has-feedback-left" max="<?php echo $m ?>" value="<?php echo $fecha ?>">
                  <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
                </div>
              </div>
            </div>
          </form>
          <hr>
          <div class="col-md-3 col-md-offset-9">
              <a class="btn btn-primary" href="<?php echo base_url() ?>Prestamo">Cancelar</a>
              <button class="btn btn-success btn_p" type="button" onclick="guadar_prestamo()">Guardar</button>
            </div>
        </div>
      </div>            
  </div>
</div>
