<style type="text/css">
  .dccliente{
    font-size: 9px;
  }
</style>
<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">
        <div class="row x_title">
          <div class="col-md-6">
            <h3>Compras <small>Listado</small></h3>
          </div>
        </div>
        <div class="col-md-12 col-sm-9 col-xs-12">
          <!--------//////////////--------> 
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label class="col-md-2 col-sm-2 col-xs-12 control-label">Tipo :</label>
                <div class="col-md-3 col-sm-3 col-xs-12 controls">
                  <select id="tipopago" class="form-control" onchange="reload_table()">
                    <option value="0">Contado</option>
                    <option value="1">Credito</option>
                  </select>
                </div>
              </div>        
            </div>
            <div class="col-md-4">
              <div class="row text-right">
                <a class="btn btn-dark float-right" href="<?php echo base_url(); ?>Compras/Comprasadd">Nuevo</a>
              </div>   
            </div>
          </div>
          <span class="text_tabla"></span>
          <!--------//////////////-------->
        </div>
        <div class="clearfix"></div>
      </div>
    </div>

  </div>
  <br />

</div>
<!-- /page content -->

<!-- /page content -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="modal_productos">
  <div class="modal-dialog modal-lg">
    <div class="modal-content curba">

      <div class="modal-header  curbaa foot">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h3 class="modal-title" id="myModalLabel2">Productos de la compra</h3>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12  viewproductos" >
            
          </div> 
          
        </div>
        

      </div>
      <div class="modal-footer  curbab foot">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>

    </div>
  </div>
</div>  

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="pago_modal">
  <div class="modal-dialog">
    <div class="modal-content curba">
      <div class="modal-header curbaa foot">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h3 class="modal-title" id="myModalLabel2">Pagos</h3>
      </div>
      <div class="modal-body">
         <div class="tabla_pagos">
           <!-- Tabla de pagos-->
         </div>
         <form class="form-horizontal form-label-left input_mask">
            <div class="form-group">
                <label class="label-text col-md-2 col-sm-2 col-xs-12">Pago</label>
                <div class="col-md-5 col-sm-5 col-xs-12">
                   <input type="number" step="any" class="form-control" id="pago">
                </div>
            </div>  
            <input type="hidden" id="comprId">
          </form>
        <br>
      </div>
      <div class="modal-footer curbab foot">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-info" onclick="btn_agregar()">Agregar</button>
      </div>
    </div>
  </div>
</div> 
