<!--<script src="<?php echo base_url(); ?>public/js/inicio.js" type="text/javascript"></script>-->
<script type="text/javascript">
  function grafica() {
            
              
            if ($('#graficaventas').length ){ 
              
              var ctx = document.getElementById("graficaventas");
              var mybarChart = new Chart(ctx, {
                type: 'bar',
                data: {
                  labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre","Octubre", "Noviembre", "Diciembre"],
                  datasets: [{
                    label: 'Total Compras',
                    backgroundColor: "#26B99A",
                    data: [
                    		<?php echo $this->ModeloReportes->gettotalcomprasmes('01',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalcomprasmes('02',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalcomprasmes('03',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalcomprasmes('04',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalcomprasmes('05',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalcomprasmes('06',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalcomprasmes('07',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalcomprasmes('08',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalcomprasmes('09',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalcomprasmes('10',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalcomprasmes('11',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalcomprasmes('12',$sucursalId);?>
                    	]
                  }, {
                    label: 'Total ventas',
                    backgroundColor: "#03586A",
                    data: [
                    		<?php echo $this->ModeloReportes->gettotalventassmes('01',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalventassmes('02',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalventassmes('03',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalventassmes('04',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalventassmes('05',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalventassmes('06',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalventassmes('07',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalventassmes('08',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalventassmes('09',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalventassmes('10',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalventassmes('11',$sucursalId);?>, 
                    		<?php echo $this->ModeloReportes->gettotalventassmes('12',$sucursalId);?>
                    	]
                  }]
                },

                options: {
                  scales: {
                    yAxes: [{
                      ticks: {
                        beginAtZero: true
                      }
                    }]
                  }
                }
              });
              
            } 
              
} 


$(document).ready(function() {
            
    grafica();
            
});

</script>
