<div class="right_col" role="main">
  <div class="col-md-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Nómina </h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="row">
            <div class="col-md-4">
              <div class="row divprinter">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">Fecha:</label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <input type="date" id="fecha" class="form-control">
                </div>
              </div>
            </div>
            <div class="col-md-5">
              <a class="btn btn-dark float-right" onclick="reload_table()">Buscar</a>
            </div>
          </div>
          <table class="table table-striped jambo_table bulk_action" id="data_tables">
            <thead>
              <tr >
                <th>#</th>
                <th>Empleado</th>
                <th>Sucursal</th>
                <th>sueldo</th>
                <th>Prestamo</th>
                <th>Total a pagar</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>            
  </div>
</div>
 