<!-- page content -->
<div class="right_col" role="main">

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">

        <div class="row x_title">
          <div class="col-md-6">
            <h3>Prestamo de cajas</h3>
          </div>
        </div>

        <div class="col-md-12 col-sm-9 col-xs-12">
          <!--------//////////////-------->
          <table class="table table-striped jambo_table bulk_action" id="data_tables">
            <thead>
              <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Celular</th>
                <th>Cajas</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tbody>
              </tbody>
            </tbody>
          </table>
          <!--------//////////////-------->
        </div>
        <div class="clearfix"></div>
      </div>
    </div>

  </div>
  <br />
</div>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true" id="cajas_modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content curba">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h3 class="modal-title" id="myModalLabel2">Devolución</h3>
      </div>
      <div class="modal-body">
        <h5>Total de cajas prestadas: <span class="numero_cajas"></span></h5>
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label class="col-md-12 col-sm-12 col-xs-12 control-label">Cajas:</label>
              <div class="col-md-12 col-sm-12 col-xs-12 controls">
                <input type="number" id="cajas" class="form-control has-feedback-left">
                <span class="fa fa-inbox form-control-feedback left" aria-hidden="true"></span>
              </div>
            </div>
          </div>
        </div> 
        <input type="hidden" id="clienteid">
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-info btn_registro" onclick="registrar_devolucion()">Registrar</button>
      </div>

    </div>
  </div>
</div>  