<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloGeneral extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    ////// Prestamo
    function getlistpretsamo($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'s.sucursal',
            3=>'p.prestamo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('personal p');   
        $this->db->join('sucursales s','s.sucursalid = p.sucursalId');
        $where = array(
            'p.activo'=>1,'p.prestamo!='=>0
        ); 
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function total_prestamo($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'s.sucursal',
            3=>'p.prestamo'
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('personal p');   
        $this->db->join('sucursales s','s.sucursalid = p.sucursalId');
        $where = array(
            'p.activo'=>1,'p.prestamo!='=>0
        ); 
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $query=$this->db->get();
        return $query->row()->total;
    }
    ////// Prestamo detalles
    function getlistpretsamo_detalles($params){
        $idempl=$params['idemp'];
        $columns = array( 
            0=>'p.idprestamo',
            1=>'p.concepto',
            2=>'p.monto',
            3=>'p.fecha'    
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('prestamo p');   
        $where = array(
            'p.activo'=>1,'p.status='=>1,'personalId'=>$idempl
        ); 
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function total_prestamo_detalles($params){
        $idempl=$params['idemp'];
        $columns = array( 
            0=>'p.idprestamo',
            1=>'p.concepto',
            2=>'p.monto',
            3=>'p.fecha'    
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('prestamo p');   
        $where = array(
            'p.activo'=>1,'p.status='=>1,'personalId'=>$idempl
        ); 
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $query=$this->db->get();
        return $query->row()->total;
    }
    ///////////// Cajas 
    function getlistcajas($params){
        $columns = array( 
            0=>'c.ClientesId',
            1=>'c.Nombre',
            2=>'c.cajas',
            3=>'c.celular',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('clientes c');   
        $where = array('c.activo'=>1,'c.cajas>'=>0); 
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function total_cajas($params){
        $columns = array( 
            0=>'c.ClientesId',
            1=>'c.Nombre',
            2=>'c.cajas',
            3=>'c.celular',
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('clientes c');   
        $where = array('c.activo'=>1,'c.cajas>'=>0); 
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $query=$this->db->get();
        return $query->row()->total;
    }
    function getlistnomina($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'p.sueldo',
            3=>'s.sucursal',
            4=>'p.prestamo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('personal p');
        $this->db->join('sucursales s','s.sucursalid = p.sucursalId');
        $where = array('p.activo'=>1); 
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function total_nomina($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'p.sueldo',
            3=>'s.sucursal',
            4=>'p.prestamo'
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('personal p');   
        $this->db->join('sucursales s','s.sucursalid = p.sucursalId');
        $where = array('p.activo'=>1); 
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $query=$this->db->get();
        return $query->row()->total;
    }
    public function get_pestamo($id,$fechai,$fechaf){
        $strq = "SELECT SUM(p.monto) as total FROM prestamo AS p WHERE p.personalId=$id AND p.activo=1 AND p.fecha BETWEEN '$fechai' AND '$fechaf'";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    public function get_validar_nomina($id,$fecha){
        $strq = "SELECT * FROM nomina AS p WHERE p.personalId=$id AND p.fecha='$fecha'";
        $query = $this->db->query($strq);
        return $query; 
    }
    ///////////// Cajas Proveedor 
    function getlistcajas_proveedor($params){
        $columns = array( 
            0=>'p.id_proveedor',
            1=>'p.razon_social',
            2=>'p.cajas',
            3=>'p.telefono_celular',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('proveedores p');   
        $where = array('p.activo'=>1,'p.cajas>'=>0); 
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function total_cajas_proveedor($params){
        $columns = array( 
            0=>'p.id_proveedor',
            1=>'p.razon_social',
            2=>'p.cajas',
            3=>'p.telefono_celular',
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('proveedores p');   
        $where = array('p.activo'=>1,'p.cajas>'=>0); 
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $query=$this->db->get();
        return $query->row()->total;
    }
    /*
    public function get_validar_nomina($id,$fecha){
        $strq = "SELECT * FROM nomina AS p WHERE p.personalId=$id AND p.fecha='$fecha'";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    */
}