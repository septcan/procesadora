<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloProductos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getproductospcompra($id){
        $strq = "SELECT preciocompra FROM productos where productoid=$id";
        $query = $this->db->query($strq);
        
        $preciocompra=0;
        
        foreach ($query->result() as $row) {
            $preciocompra =$row->preciocompra;
        } 
        return $preciocompra;

    }
    function numproductos(){
        $strq = "SELECT count(*) as total FROM productos where activo=1";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function numproductospermitidos(){
        $strq = "SELECT productosmaximos FROM configuracion where id=1";
        $query = $this->db->query($strq);
        $productosmaximos=0;
        foreach ($query->result() as $row) {
            $productosmaximos =$row->productosmaximos;
        } 
        return $productosmaximos;
    }
    function getcategoriasactive(){
        $strq="SELECT pro.categoria as idcat,cat.categoria 
            FROM productos as pro
            inner JOIN categoria as cat on cat.categoriaId=pro.categoria
            WHERE pro.activo=1
            GROUP BY pro.categoria";
        $query = $this->db->query($strq);
        return $query->result();
    }
    function producto_existencia($idpro,$idsucu){
        $strq = "SELECT * FROM productos_sucursales WHERE idproducto =$idpro and idsucursal=$idsucu";
        $query = $this->db->query($strq);
        $idproductosucursal=0;
        $precio_venta='';
        $mayoreo='';
        $can_mayoreo='';
        $existencia='';
        
        foreach ($query->result() as $item) {
            $idproductosucursal=$item->id;
            $precio_venta=$item->precio_venta;
            $mayoreo=$item->mayoreo;
            $can_mayoreo=$item->can_mayoreo;
            $existencia=$item->existencia;
        }
        $arrayproductosucursal = array(
                            'idproductosucursal' =>$idproductosucursal,
                            'precio_venta' =>$precio_venta,
                            'mayoreo' =>$mayoreo,
                            'can_mayoreo' =>$can_mayoreo,
                            'existencia' =>$existencia,
                           );
        return $arrayproductosucursal; 
    }
    


}