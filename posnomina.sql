-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 17-09-2020 a las 03:55:00
-- Versión del servidor: 5.7.26
-- Versión de PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `posnomina`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `backup`
--

DROP TABLE IF EXISTS `backup`;
CREATE TABLE IF NOT EXISTS `backup` (
  `backupid` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `personalId` int(11) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activo` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`backupid`),
  KEY `backup_fk_personal` (`personalId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

DROP TABLE IF EXISTS `bitacora`;
CREATE TABLE IF NOT EXISTS `bitacora` (
  `bitacoraid` bigint(20) NOT NULL AUTO_INCREMENT,
  `personalId` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `id_cambio` int(11) NOT NULL,
  `tipo_cambio` varchar(200) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`bitacoraid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

DROP TABLE IF EXISTS `categoria`;
CREATE TABLE IF NOT EXISTS `categoria` (
  `categoriaId` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(200) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`categoriaId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`categoriaId`, `categoria`, `activo`, `reg`) VALUES
(1, 'Ninguno', 1, '2019-04-08 01:20:09'),
(2, 'Lacteos', 1, '2019-06-24 00:49:24'),
(3, 'Cereales', 1, '2019-06-24 01:14:35'),
(4, 'Aceite Comestible', 1, '2019-06-25 13:33:49'),
(5, 'Café y Té', 1, '2019-06-25 13:40:14'),
(6, 'Embutidos', 1, '2019-06-25 13:43:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `ClientesId` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(250) NOT NULL,
  `Domicilio` text,
  `Ciudad` varchar(100) DEFAULT NULL,
  `Estado` varchar(100) DEFAULT NULL COMMENT 'Estado/Provincia/Region',
  `Pais` varchar(45) DEFAULT NULL,
  `CodigoPostal` varchar(45) DEFAULT NULL,
  `Correo` varchar(200) NOT NULL,
  `nombrec` varchar(100) NOT NULL,
  `correoc` varchar(200) NOT NULL,
  `telefonoc` varchar(30) NOT NULL,
  `extencionc` varchar(20) NOT NULL,
  `celular` varchar(30) NOT NULL,
  `descripcionc` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '1 activo 0 eliminado',
  `propietarioid` int(11) NOT NULL,
  `cajas` float NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ClientesId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`ClientesId`, `Nombre`, `Domicilio`, `Ciudad`, `Estado`, `Pais`, `CodigoPostal`, `Correo`, `nombrec`, `correoc`, `telefonoc`, `extencionc`, `celular`, `descripcionc`, `activo`, `propietarioid`, `cajas`, `reg`) VALUES
(1, 'PUBLICO EN GENERAL', '', '', '', 'Mexico', '', '', '', '', '', '', '', '', 1, 0, 0, '2019-04-26 22:24:22'),
(2, 'Noe Rosales', '', '', '', NULL, '', '', '', '', '', '', '', '', 1, 0, 90, '2020-08-25 14:18:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

DROP TABLE IF EXISTS `compras`;
CREATE TABLE IF NOT EXISTS `compras` (
  `compraId` int(11) NOT NULL AUTO_INCREMENT,
  `sucursalid` int(11) NOT NULL DEFAULT '1',
  `id_proveedor` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `monto_total` float NOT NULL,
  `cajas_total` float NOT NULL,
  `tipopago` int(1) NOT NULL COMMENT '0-contado 1-credito',
  `montocredito` float NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`compraId`),
  KEY `compra_fk_proveedor` (`id_proveedor`),
  KEY `compra_fk_personal` (`personalId`),
  KEY `compras_fk_sucursal` (`sucursalid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_detalles`
--

DROP TABLE IF EXISTS `compras_detalles`;
CREATE TABLE IF NOT EXISTS `compras_detalles` (
  `compradId` int(11) NOT NULL AUTO_INCREMENT,
  `compraId` int(11) NOT NULL,
  `productoid` bigint(20) NOT NULL,
  `cantidad` float NOT NULL,
  `precio_compra` float NOT NULL,
  `cajas` float NOT NULL,
  PRIMARY KEY (`compradId`),
  KEY `compra_fk_compra` (`compraId`),
  KEY `compra_fk_productos` (`productoid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

DROP TABLE IF EXISTS `configuracion`;
CREATE TABLE IF NOT EXISTS `configuracion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuariosmaximo` int(2) NOT NULL DEFAULT '4',
  `productosmaximos` int(11) NOT NULL DEFAULT '4000',
  `vigencia` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `costomensual` float NOT NULL COMMENT '3% + 3 pesos',
  `costoanual` int(11) NOT NULL COMMENT '3% + 3 pesos',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id`, `usuariosmaximo`, `productosmaximos`, `vigencia`, `costomensual`, `costoanual`) VALUES
(1, 4, 4000, '2020-10-31 22:26:51', 240, 2578);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `devolucioncajas`
--

DROP TABLE IF EXISTS `devolucioncajas`;
CREATE TABLE IF NOT EXISTS `devolucioncajas` (
  `idcajas` bigint(20) NOT NULL AUTO_INCREMENT,
  `ClientesId` bigint(20) NOT NULL,
  `cajas` float NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `detalles` text NOT NULL,
  `reg` datetime DEFAULT NULL,
  PRIMARY KEY (`idcajas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `devolucioncajas_proveedor`
--

DROP TABLE IF EXISTS `devolucioncajas_proveedor`;
CREATE TABLE IF NOT EXISTS `devolucioncajas_proveedor` (
  `idcajas` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_proveedor` bigint(20) NOT NULL,
  `cajas` float NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `detalles` text NOT NULL,
  `reg` datetime DEFAULT NULL,
  PRIMARY KEY (`idcajas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

DROP TABLE IF EXISTS `gastos`;
CREATE TABLE IF NOT EXISTS `gastos` (
  `gastosid` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `cantidad` float NOT NULL,
  `concepto` text NOT NULL,
  `sucursal` int(11) NOT NULL,
  `comentarios` text NOT NULL,
  `personalId` int(11) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT '1' COMMENT '1 vigente 0 cancelado',
  `personalId_cancela` int(11) DEFAULT NULL,
  `motivo_cancela` text,
  `reg_cancela` timestamp NULL DEFAULT NULL,
  `activod` int(1) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`gastosid`),
  KEY `gastos_fk_sucursal` (`sucursal`),
  KEY `gastos_fk_personal` (`personalId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `MenuId` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `Icon` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`MenuId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`MenuId`, `Nombre`, `Icon`) VALUES
(1, 'Catálogos', 'fa fa-book'),
(2, 'Operaciones', 'fa fa-folder-open'),
(3, 'Configuración\n', 'fa fa fa-cogs');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_sub`
--

DROP TABLE IF EXISTS `menu_sub`;
CREATE TABLE IF NOT EXISTS `menu_sub` (
  `MenusubId` int(11) NOT NULL AUTO_INCREMENT,
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Pagina` varchar(120) CHARACTER SET latin1 DEFAULT NULL,
  `Icon` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`MenusubId`),
  KEY `fk_menu_sub_menu_idx` (`MenuId`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menu_sub`
--

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES
(1, 1, 'Personal', 'Personal', 'fa fa-group'),
(2, 3, 'Categoria', 'Categoria', 'fa fa-cogs'),
(3, 1, 'Productos', 'Productos', 'fa fa-group'),
(4, 2, 'Ventas', 'Ventas', 'fa fa-shopping-cart'),
(5, 2, 'Compras', 'Compras', 'fa fa-cogs'),
(6, 1, 'Clientes', 'Clientes', 'fa fa-user'),
(7, 1, 'Proveedores', 'Proveedores', 'fa fa-truck'),
(8, 2, 'Reportes', 'Reportes', 'fa fa-cogs'),
(9, 2, 'Lista de ventas', '-', 'fa fa-cogs'),
(10, 2, 'Turno', '-', 'fa fa-cogs'),
(11, 2, 'Lista de turnos', '-', 'fa fa-cogs'),
(12, 2, 'Lista de compras', '-', 'fa fa-shopping-cart'),
(13, 3, 'Config. de ticket', 'Config_ticket', 'fa fa-cogs'),
(14, 2, 'Verificador', 'Verificador', 'fa fa-eye'),
(15, 2, 'Gastos', 'Gastos', 'fa fa-money'),
(16, 1, 'Sucursal', 'Sucursal', 'fa fa-briefcase'),
(17, 2, 'Listado Ventas Normales', 'ListadoVentasNormales', 'fa fa-bar-chart'),
(18, 2, 'Listado Ventas Credito', 'ListadoVentasCredito', 'fa fa-bar-chart'),
(19, 3, 'Usuarios', 'Usuarios', 'fa fa-user'),
(20, 3, 'Pagos', 'Pagos', 'fa fa-cogs'),
(21, 3, 'Respaldos', 'Respaldos', 'fa fa-download'),
(22, 2, 'Prestamo', 'Prestamo', '	\r\nfa fa-money'),
(23, 2, 'Nómina', 'Nomina', 'fa fa-money'),
(24, 2, 'Cajas Clientes', 'Cajas', 'fa fa-inbox'),
(25, 2, 'Cajas Proveedor', 'CajasProveedor ', 'fa fa-inbox');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina`
--

DROP TABLE IF EXISTS `nomina`;
CREATE TABLE IF NOT EXISTS `nomina` (
  `idnomina` int(11) NOT NULL AUTO_INCREMENT,
  `personalId` int(11) NOT NULL,
  `sueldo` float NOT NULL,
  `descuento_prestamo` float NOT NULL,
  `total` float NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`idnomina`),
  KEY `nomina_fk_personal` (`personalId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos_credito`
--

DROP TABLE IF EXISTS `pagos_credito`;
CREATE TABLE IF NOT EXISTS `pagos_credito` (
  `pagoId` int(11) NOT NULL AUTO_INCREMENT,
  `ventaId` bigint(20) NOT NULL,
  `personalId` int(11) NOT NULL,
  `pago` float NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pagoId`),
  KEY `pago_fk_venta` (`ventaId`),
  KEY `pago_fk_personal` (`personalId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos_credito_compras`
--

DROP TABLE IF EXISTS `pagos_credito_compras`;
CREATE TABLE IF NOT EXISTS `pagos_credito_compras` (
  `pagoId` int(11) NOT NULL AUTO_INCREMENT,
  `compraId` bigint(20) NOT NULL,
  `personalId` int(11) NOT NULL,
  `pago` float NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pagoId`),
  KEY `pago_fk_compras` (`compraId`),
  KEY `pago_fk_personal` (`personalId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

DROP TABLE IF EXISTS `perfiles`;
CREATE TABLE IF NOT EXISTS `perfiles` (
  `perfilId` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`perfilId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`perfilId`, `nombre`) VALUES
(1, 'Administrador root'),
(2, 'Administrador'),
(3, 'Ventas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles_detalles`
--

DROP TABLE IF EXISTS `perfiles_detalles`;
CREATE TABLE IF NOT EXISTS `perfiles_detalles` (
  `Perfil_detalleId` int(11) NOT NULL AUTO_INCREMENT,
  `perfilId` int(11) NOT NULL,
  `MenusubId` int(11) NOT NULL,
  PRIMARY KEY (`Perfil_detalleId`),
  KEY `fk_Perfiles_detalles_Perfiles1_idx` (`perfilId`),
  KEY `fk_Perfiles_detalles_menu_sub1_idx` (`MenusubId`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `perfiles_detalles`
--

INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 7),
(5, 1, 6),
(6, 1, 4),
(7, 1, 13),
(8, 1, 14),
(9, 1, 15),
(10, 1, 16),
(11, 1, 17),
(12, 1, 18),
(13, 1, 5),
(14, 1, 8),
(15, 1, 19),
(17, 2, 1),
(18, 2, 2),
(19, 2, 3),
(20, 2, 7),
(21, 2, 6),
(22, 2, 4),
(23, 2, 14),
(24, 2, 15),
(25, 2, 17),
(26, 2, 18),
(27, 2, 5),
(28, 2, 8),
(29, 2, 19),
(30, 3, 4),
(31, 3, 5),
(32, 3, 14),
(33, 3, 15),
(34, 1, 21),
(35, 1, 22),
(36, 1, 23),
(37, 1, 24),
(38, 1, 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

DROP TABLE IF EXISTS `personal`;
CREATE TABLE IF NOT EXISTS `personal` (
  `personalId` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) NOT NULL,
  `domicilio` text NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `tipo` int(1) NOT NULL DEFAULT '1' COMMENT '0 administrador 1 normal',
  `sucursalId` int(11) NOT NULL,
  `prestamo` float NOT NULL,
  `sueldo` float NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '1 visible 0 eliminado',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`personalId`),
  KEY `personal_fk_sucursal` (`sucursalId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`personalId`, `nombre`, `domicilio`, `telefono`, `celular`, `correo`, `tipo`, `sucursalId`, `prestamo`, `sueldo`, `activo`, `reg`) VALUES
(1, 'Administrador', '', '', '', '', 0, 1, 0, 0, 1, '2019-03-18 20:10:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamo`
--

DROP TABLE IF EXISTS `prestamo`;
CREATE TABLE IF NOT EXISTS `prestamo` (
  `idprestamo` bigint(20) NOT NULL AUTO_INCREMENT,
  `personalId` int(11) NOT NULL,
  `concepto` text NOT NULL,
  `monto` float NOT NULL,
  `fecha` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1-no cobrado 0-cobrado',
  `activo` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idprestamo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

DROP TABLE IF EXISTS `productos`;
CREATE TABLE IF NOT EXISTS `productos` (
  `productoid` bigint(20) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(20) NOT NULL,
  `nombre` varchar(120) NOT NULL,
  `descripcion` text NOT NULL,
  `categoria` int(11) NOT NULL,
  `stock1` float NOT NULL DEFAULT '0',
  `stock2` float NOT NULL DEFAULT '0',
  `stock3` float NOT NULL DEFAULT '0',
  `preciocompra` float NOT NULL,
  `precioventa` float NOT NULL,
  `img` varchar(120) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '1 actual 0 eliminado',
  `propietarioid` int(11) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`productoid`),
  KEY `producto_fk_categoria` (`categoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_sucursales`
--

DROP TABLE IF EXISTS `productos_sucursales`;
CREATE TABLE IF NOT EXISTS `productos_sucursales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idsucursal` int(11) NOT NULL,
  `idproducto` bigint(20) NOT NULL,
  `precio_venta` float NOT NULL,
  `mayoreo` float NOT NULL,
  `can_mayoreo` float NOT NULL,
  `existencia` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `producto_fk_sucursal` (`idproducto`),
  KEY `prosuc_fk_sucursal` (`idsucursal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
CREATE TABLE IF NOT EXISTS `proveedores` (
  `id_proveedor` int(11) NOT NULL AUTO_INCREMENT,
  `razon_social` varchar(100) NOT NULL,
  `domicilio` varchar(100) NOT NULL COMMENT 'calle',
  `ciudad` varchar(50) NOT NULL,
  `estado` varchar(200) NOT NULL COMMENT 'estado, provincia, etc',
  `cp` varchar(8) NOT NULL,
  `telefono_local` varchar(10) NOT NULL,
  `telefono_celular` varchar(15) NOT NULL,
  `contacto` varchar(100) NOT NULL,
  `email_contacto` varchar(60) NOT NULL,
  `rfc` varchar(13) NOT NULL,
  `obser` text NOT NULL,
  `cajas` float NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_proveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursales`
--

DROP TABLE IF EXISTS `sucursales`;
CREATE TABLE IF NOT EXISTS `sucursales` (
  `sucursalid` int(11) NOT NULL AUTO_INCREMENT,
  `logo` text NOT NULL,
  `sucursal` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `direccion` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`sucursalid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sucursales`
--

INSERT INTO `sucursales` (`sucursalid`, `logo`, `sucursal`, `direccion`, `telefono`, `activo`) VALUES
(1, '200818-034435cat4.jpg', 'Procesadora', 'av sur ', '22222222222', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

DROP TABLE IF EXISTS `ticket`;
CREATE TABLE IF NOT EXISTS `ticket` (
  `id_ticket` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` text NOT NULL,
  `mensajea` text NOT NULL,
  `mensajeb` text NOT NULL,
  `fuente` varchar(20) NOT NULL,
  `margen_superior` int(11) NOT NULL,
  `tamanio` int(11) NOT NULL,
  `barcode` int(11) NOT NULL,
  `anchot` int(11) NOT NULL,
  PRIMARY KEY (`id_ticket`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ticket`
--

INSERT INTO `ticket` (`id_ticket`, `titulo`, `mensajea`, `mensajeb`, `fuente`, `margen_superior`, `tamanio`, `barcode`, `anchot`) VALUES
(1, 'Procesadora', '<p>QUEJAS Y SUJERENCIAS 01 800 000 0000</p>\r\n', '<p>Como te atendimos?</p>\r\n\r\n<p>YA AHORRASTE CON LOS PRECIOS MAS BAJOS</p>\r\n', 'courier', 10, 9, 19, 80);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket_bar`
--

DROP TABLE IF EXISTS `ticket_bar`;
CREATE TABLE IF NOT EXISTS `ticket_bar` (
  `barId` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `codigo` varchar(100) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`barId`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ticket_bar`
--

INSERT INTO `ticket_bar` (`barId`, `nombre`, `codigo`, `activo`) VALUES
(1, 'CODABAR', 'CODABAR', 1),
(2, 'CODE 39 - ANSI MH10.8M-1983 - USD-3 - 3 of 9', 'C39', 1),
(3, 'CODE 39 + CHECKSUM', 'C39+', 1),
(4, 'CODE 39 EXTENDED', 'C39E', 1),
(5, 'CODE 39 EXTENDED + CHECKSUM', 'C39E+', 1),
(6, 'CODE 93 - USS-93', 'C93', 1),
(7, 'Standard 2 of 5', 'S25', 1),
(8, 'Standard 2 of 5 + CHECKSUM', 'S25+', 1),
(9, 'Interleaved 2 of 5', 'I25', 1),
(10, 'Interleaved 2 of 5 + CHECKSUM', 'I25+', 1),
(11, 'CODE 128 AUTO', 'C128', 1),
(12, 'CODE 128 A', 'C128A', 1),
(13, 'CODE 128 B', 'C128B', 1),
(14, 'CODE 128 C', 'C128C', 1),
(15, 'EAN 8', 'EAN8', 1),
(16, 'EAN 13', 'EAN13', 1),
(17, 'UPC-A', 'UPCA', 1),
(18, 'UPC-E', 'UPCE', 1),
(19, 'QRCODE,L', 'QRCODE,L', 1),
(20, 'QRCODE,M', 'QRCODE,M', 1),
(21, 'QRCODE,Q', 'QRCODE,Q', 1),
(22, 'QRCODE,H', 'QRCODE,H', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `UsuarioID` int(11) NOT NULL AUTO_INCREMENT,
  `perfilId` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `Usuario` varchar(45) DEFAULT NULL,
  `contrasena` varchar(80) CHARACTER SET latin1 DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`UsuarioID`),
  KEY `fk_usuarios_Perfiles1_idx` (`perfilId`),
  KEY `fk_usuarios_personal1_idx` (`personalId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`UsuarioID`, `perfilId`, `personalId`, `Usuario`, `contrasena`, `status`) VALUES
(1, 1, 1, 'admin', '$2y$10$.yBpfsTTEmhYhECki.qy3eL45XaLo3MinuIPgiPsjmXBYItZXHUga', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

DROP TABLE IF EXISTS `ventas`;
CREATE TABLE IF NOT EXISTS `ventas` (
  `ventaId` bigint(20) NOT NULL AUTO_INCREMENT,
  `sucursalid` int(11) NOT NULL DEFAULT '1',
  `personalId` int(11) NOT NULL,
  `ClientesId` bigint(20) NOT NULL,
  `subtotal` float NOT NULL,
  `ndescuento` float NOT NULL,
  `descuento` float NOT NULL,
  `total` float NOT NULL,
  `tipopago` int(1) NOT NULL DEFAULT '1' COMMENT '1 contado 2 credito',
  `fechavencimiento` date DEFAULT NULL COMMENT 'la fecha de vencimiento en caso de ser credito',
  `pagado` int(1) NOT NULL DEFAULT '1' COMMENT '1 pagado, 0 no cubierto en el caso de los creditos',
  `metodo` int(1) NOT NULL DEFAULT '1' COMMENT '1 efectivo, 2 credito, 3 debito',
  `cancelado` int(1) NOT NULL DEFAULT '0',
  `cancela_personal` int(11) DEFAULT NULL,
  `cancela_motivo` text,
  `canceladoh` timestamp NULL DEFAULT NULL,
  `taras` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ventaId`),
  KEY `ventas_fk_personal` (`personalId`),
  KEY `ventas_fk_clientes` (`ClientesId`),
  KEY `ventas_fk_sucursal` (`sucursalid`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_detalle`
--

DROP TABLE IF EXISTS `venta_detalle`;
CREATE TABLE IF NOT EXISTS `venta_detalle` (
  `ventadId` bigint(20) NOT NULL AUTO_INCREMENT,
  `ventaId` bigint(20) NOT NULL,
  `productoid` bigint(20) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL,
  `precioc` float NOT NULL COMMENT 'es el precio de compra en el momento de que se realizo la venta',
  PRIMARY KEY (`ventadId`),
  KEY `detallev_fk_productos` (`productoid`),
  KEY `ventasd_fk_ventas` (`ventaId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `backup`
--
ALTER TABLE `backup`
  ADD CONSTRAINT `backup_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `compra_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `compra_fk_proveedor` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id_proveedor`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `compras_fk_sucursal` FOREIGN KEY (`sucursalid`) REFERENCES `sucursales` (`sucursalid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compras_detalles`
--
ALTER TABLE `compras_detalles`
  ADD CONSTRAINT `compra_fk_compra` FOREIGN KEY (`compraId`) REFERENCES `compras` (`compraId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `compra_fk_productos` FOREIGN KEY (`productoid`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD CONSTRAINT `gastos_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `gastos_fk_sucursal` FOREIGN KEY (`sucursal`) REFERENCES `sucursales` (`sucursalid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `nomina`
--
ALTER TABLE `nomina`
  ADD CONSTRAINT `nomina_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pagos_credito`
--
ALTER TABLE `pagos_credito`
  ADD CONSTRAINT `pago_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `pago_fk_venta` FOREIGN KEY (`ventaId`) REFERENCES `ventas` (`ventaId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD CONSTRAINT `perfil_fk_menu` FOREIGN KEY (`MenusubId`) REFERENCES `menu_sub` (`MenusubId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `perfil_fk_perfil` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personal`
--
ALTER TABLE `personal`
  ADD CONSTRAINT `personal_fk_sucursal` FOREIGN KEY (`sucursalId`) REFERENCES `sucursales` (`sucursalid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `producto_fk_categoria` FOREIGN KEY (`categoria`) REFERENCES `categoria` (`categoriaId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos_sucursales`
--
ALTER TABLE `productos_sucursales`
  ADD CONSTRAINT `producto_fk_sucursal` FOREIGN KEY (`idproducto`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `prosuc_fk_sucursal` FOREIGN KEY (`idsucursal`) REFERENCES `sucursales` (`sucursalid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuario_fk_perfil` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `usuario_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `ventas_fk_clientes` FOREIGN KEY (`ClientesId`) REFERENCES `clientes` (`ClientesId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `ventas_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `ventas_fk_sucursal` FOREIGN KEY (`sucursalid`) REFERENCES `sucursales` (`sucursalid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD CONSTRAINT `detallev_fk_productos` FOREIGN KEY (`productoid`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `ventasd_fk_ventas` FOREIGN KEY (`ventaId`) REFERENCES `ventas` (`ventaId`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
