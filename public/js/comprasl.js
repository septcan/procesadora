var base_url=$('#base_url').val();
var table;
$(document).ready(function() {
  loadtable(0);
});
function reload_table(){
  var tipo=$('#tipopago option:selected').val();
  if(tipo==0){
    loadtable(0);
  }else{
    loadtable(1);
  }
}
function reload_tabla(){
  table.destroy();
  var tipo=$('#tipopago option:selected').val();
  if(tipo==0){
    loadtable(0);
  }else{
    loadtable(1);
  }
}
function loadtable(id){
    $('.text_tabla').html('');
    var tablatex='<table class="table table-striped jambo_table bulk_action" id="data_tables">\
            <thead>\
              <tr >\
                <th>#</th>\
                <th>Proveedor</th>\
                <th>Monto</th>';
              if(id==1){  
      tablatex+='<th>Debe</th>';
              }         
      tablatex+='<th>Fecha</th>';
              if(id==1){  
      tablatex+='<th></th>';
              }         
      tablatex+='<th></th>\
              </tr>\
            </thead>\
            <tbody>\
            </tbody>\
          </table>';
    $('.text_tabla').html(tablatex);
    if(id==0){
      table=$('#data_tables').DataTable({
          fixedHeader: true,
          responsive: !0,
          "bProcessing": true,
          "serverSide": true,
          "ajax": {
              url: base_url+"Compras/getData_listado",
              type: "post",
              data:{tipopago: id},
              error: function() {
                  $("#data_tables").css("display", "none");
              }
          },
          "columns": [
              {"data": "compraId"},  
              {"data": "razon_social"}, 
              {"data": "monto_total"},
              {"data": "reg"}, 
              {"data": null,
                  "render": function(data, type, row, meta ) {
                      var html='<button type="button" class="btn btn-info" onclick="productoscompras('+row.compraId+')">Productos</button>\
                          <a class="btn btn-default" href="'+base_url+'Reportes/ticket_compras/'+row.compraId+'" target="_blank" title="Ticket"><i class="fa fa-ticket"></i></a>';
                      return html;
                  }
              }, 
          ],
          order: [
              [0, "desc"]
          ],
      });
    }else{
      table=$('#data_tables').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url+"Compras/getData_listado",
            type: "post",
            data:{tipopago: id},
            error: function() {
                $("#data_tables").css("display", "none");
            }
        },
        "columns": [
            {"data": "compraId"},  
            {"data": "razon_social"}, 
            {"data": "monto_total"},
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html='';
                    if(row.montocredito<=0){
                      html='<a class=" btn-success btn-sm">Cubierto</a>';
                    }else{
                      html=row.montocredito;
                    }
                    return html;
                }
            }, 
            {"data": "reg"}, 
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html='';
                    if(row.montocredito>0){
                      html='<button type="button" class="btn btn-primary productostactiles id_compr_'+row.compraId+'" data-totaldebe="'+row.montocredito+'" onclick="agregar_pago('+row.compraId+')">Agregar pago</button>';
                    }else{
                      html='';
                    }
                    return html;
                }
            }, 
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html='<button type="button" class="btn btn-info" onclick="productoscompras('+row.compraId+')">Productos</button>\
                        <a class="btn btn-default" href="'+base_url+'Reportes/ticket_compras/'+row.compraId+'" target="_blank" title="Ticket"><i class="fa fa-ticket"></i></a>';
                    return html;
                }
            }, 
        ],
        order: [
            [0, "desc"]
        ],
    });
    }
}

function productoscompras(id){
  $('#modal_productos').modal();
  var base_url = $('#base_url').val();
  $.ajax({
      type: 'POST',
      url: base_url + 'Compras/compraproductos',
      data: {
          compraid: id
      },
      async: false,
      statusCode: {
          404: function(data) {
              new PNotify({
                  title: 'Error!',
                  text: 'No Se encuentra el archivo',
                  type: 'error',
                  styling: 'bootstrap3'
              });
          },
          500: function() {
              new PNotify({
                  title: 'Error!',
                  text: 'Error 500',
                  type: 'error',
                  styling: 'bootstrap3'
              });
          }
      },
      success: function(data) {
        $('.viewproductos').html('');
        $('.viewproductos').html(data);
        $('#tableproductoscompras').DataTable({
          "lengthChange": false
        });
      }
  });
}
function agregar_pago(id){
  $('#pago_modal').modal();
  $('#comprId').val(id);
  $.ajax({ 
      type:'POST',
      url: base_url+'Compras/pagos',
      data: {compId:id},
      async: false,
      statusCode:{
          404: function(data){
              new PNotify({
                    title: 'Error!',
                    text: 'No se encuentra archivo',
                    type: 'error',
                    styling: 'bootstrap3'
                });

          },
          500: function(){
              new PNotify({
                    title: 'Error!',
                    text: '500',
                    type: 'error',
                    styling: 'bootstrap3'
                });
          }
      },
      success:function(data){
            $('.tabla_pagos').html(data); 
      }
  });
}
function btn_agregar(){
  var pago = $('#pago').val();
  var id=$('#comprId').val();
  var ttp=$('.id_compr_'+id).data('totaldebe');
  var pagos=parseFloat(pago);
  var ttpa=parseFloat(ttp);
  if(pago!=''){
      if(pagos<=ttpa){
        $.ajax({ 
          type:'POST',
          url: base_url+'Compras/add',
          data: {comprId:$("#comprId").val(),pago:$('#pago').val()},
          async: false,
          statusCode:{
              404: function(data){
                  new PNotify({
                        title: 'Error!',
                        text: 'No se encuentra archivo',
                        type: 'error',
                        styling: 'bootstrap3'
                    });

              },
              500: function(){
                  new PNotify({
                        title: 'Error!',
                        text: '500',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
              }
          },
          success:function(data){
               if(data == 1){
                  new PNotify({
                        title: 'Verificar!',
                         text: 'Es mayor a la cantidad que debe',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
               }else{
                new PNotify({
                        title: 'Hecho!',
                         text: 'Guardado Correctamente',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
               setTimeout(function(){ 
                   $('#pago_modal').modal('hide');
                  }, 1000);
               }
                reload_tabla();
          }
        });
      }else if(pagos>ttpa){
          new PNotify({
            title: 'Atencion!',
            text: 'La cantidad es mayor a lo que debe',
            type: 'error',
            styling: 'bootstrap3'
          });
          ca=$('#cajas').val('');
      } 
  }else{
    new PNotify({
        title: 'Verificar!',
         text: 'Ingrese una cantidad',
        type: 'error',
        styling: 'bootstrap3'
    });
  }
  
}