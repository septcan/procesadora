var base_url = $('#base_url').val();
var idsucu=$('#sucursalId option:selected').val();
$(document).ready(function() {

	$('#personalId').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Empleado',
        ajax: {
            url: base_url + 'Prestamo/searchempleado/'+idsucu,
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public',
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
});
function guadar_prestamo(){
    var form_register = $('#form_prestamo');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);

    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            personalId:{
              required: true
            },
            monto:{
              required: true
            },

        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var valid = $('#form_prestamo').valid();
    if(valid) {
        var datos=$('#form_prestamo').serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Prestamo/addregistro',
            data: datos,
            async: false,
            statusCode:{
                404: function(data){
                    new PNotify({
                          title: 'Error!',
                          text: 'No se encuentra archivo',
                          type: 'error',
                          styling: 'bootstrap3'
                      });

                },
                500: function(){
                    new PNotify({
                          title: 'Error!',
                          text: '500',
                          type: 'error',
                          styling: 'bootstrap3'
                      });
                }
            },
            success:function(data){
                 new PNotify({
                          title: 'Hecho!',
                          text: 'Guardado Correctamente',
                          type: 'success',
                          styling: 'bootstrap3'
                      });
                setInterval(function(){ 
                location.href=base_url+'Prestamo';
                }, 3000);    
            }
        });
        $(".btn_p").prop('disabled', true);
    }
}