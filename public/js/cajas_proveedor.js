var base_url=$('#base_url').val();
var table;
$(document).ready(function() {
	loadtable();
});
function loadtable(){
    table=$('#data_tables').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url+"CajasProveedor/getData_listado",
            type: "post",
            error: function() {
                $("#data_tables").css("display", "none");
            }
        },
        "columns": [
            {"data": "id_proveedor"}, 
            {"data": "razon_social"}, 
            {"data": "telefono_celular"}, 
            {"data": "cajas"},  
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html='<button type="button" class="btn btn-dark id_pro_'+row.id_proveedor+'" data-totalcajas="'+row.cajas+'" onclick="modal_devolucion('+row.id_proveedor+')">Ingresar devolucion</button>'; 
                    return html;
                }
            },  
        ],
        order: [
            [0, "desc"]
        ],
    });
}
function reload_table(){
    table.destroy();
    loadtable();
}
function modal_devolucion(id){
    $('#cajas_modal').modal();
    var ttc=$('.id_pro_'+id).data('totalcajas');
    $('.numero_cajas').html(ttc);
    $('#cajas').val('');
    $('#idproveedor').val(id);
}

function registrar_devolucion(){
    var id=$('#idproveedor').val();
    var ca=$('#cajas').val();
    var ttc=$('.id_pro_'+id).data('totalcajas');
    var caj=parseFloat(ca);
    var ttca=parseFloat(ttc);
    if(ca!=''){
        if(caj<=ttca){
            $('.btn_registro').prop('disabled',true);
            $.ajax({
                type:'POST',
                url: base_url+'CajasProveedor/add',
                data: {idc:$('#idproveedor').val(),cajas:$('#cajas').val()},
                async: false,
                statusCode:{
                    404: function(data){
                        new PNotify({
                              title: 'Error!',
                              text: 'No se encuentra archivo',
                              type: 'error',
                              styling: 'bootstrap3'
                          });

                    },
                    500: function(){
                        new PNotify({
                              title: 'Error!',
                              text: '500',
                              type: 'error',
                              styling: 'bootstrap3'
                          });
                    }
                },
                success:function(data){
                    new PNotify({
                              title: 'Hecho!',
                              text: 'Guardado Correctamente',
                              type: 'success',
                              styling: 'bootstrap3'
                          });
                    setTimeout(function(){ 
                    $('#cajas_modal').modal('hide');
                    reload_table();
                    $('.btn_registro').prop('disabled',false);
                    $('#cajas').val('')
                    }, 1000);                        
                }
            });
        }else if(caj>ttca){
            new PNotify({
              title: 'Atencion!',
              text: 'Las cajas son mayor a las que debe',
              type: 'error',
              styling: 'bootstrap3'
            });
            ca=$('#cajas').val('');
        } 
    }else{
        new PNotify({
              title: 'Atención!',
              text: 'Tienes que llenar el campo de cajas',
              type: 'error',
              styling: 'bootstrap3'
        });
    }
}
