var base_url = $('#base_url').val();
var tabledata;
var tabledata_detalles;
$(document).ready(function() {
	$('#personalId').select2({
        width: '100px',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Empleado',
        ajax: {
            url: base_url + 'Prestamo/searchempleado',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    loadtable();
});
function loadtable() {
    tabledata = $('#data_tables').DataTable({
        fixedHeader: true,
        responsive: 0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "Prestamo/getData",
            type: "post",
        },
        "columns": [
            {"data": "personalId"},
            {"data": "nombre"}, 
            {"data": "sucursal"},
            {"data": null,
                "render": function(data, type, row, meta ) {
                const usCurrencyFormat = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD'});
                const usCurrency = usCurrencyFormat.format(row.prestamo);
                var html=usCurrency;
                    return html;
                }
            },
            {"data": null,
                "render": function(data, type, row, meta){
                    var html='<a type="button" class="btn btn-default emple_'+row.personalId+'" data-empleado="'+row.nombre+'" onclick="modal_detalles('+row.personalId+')">Detalles</a>';

                    /*
                    var html = '<a href="'+base_url+'Prestamo/addprestamo/'+row.personalId+'" class="btn btn-round btn-success"><i class="fa fa-edit"></i></a>\
                                <button type="button" class="btn btn-round btn-danger nombre_p'+row.personalId+'" onclick="modal_eliminar('+row.personalId+')"><i class="fa fa-trash"></i></button>';
                    */
                    return html;
                }
            },
          ],
        order: [
            [0, "desc"]
          ],
    });
}
function modal_detalles(id){
    $('#modal_detalles').modal();
    $('.empleado_text').html($('.emple_'+id).data('empleado'));
    loadtable_detalles(id);
}
function loadtable_detalles(id){
    $('.tabla_prestamos').html('<table style="width: 100%;" class="table table-striped jambo_table bulk_action" id="data_tables_prestamo">\
                                <thead>\
                                  <tr>\
                                    <th>#</th>\
                                    <th>Concepto</th>\
                                    <th>Monto</th>\
                                    <th>Fecha</th>\
                                    <th></th>\
                                  </tr>\
                                </thead>\
                                <tbody>\
                                </tbody>\
                              </table>');
    tabledata_detalles = $('#data_tables_prestamo').DataTable({
        fixedHeader: true,
        responsive: 0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "Prestamo/getprestamo",
            type: "post",
            data:{idemp:id},
        },
        "columns": [
            {"data": "idprestamo"}, 
            {"data": "concepto"}, 
            {"data": null,
                "render": function(data, type, row, meta ) {
                const usCurrencyFormat = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD'});
                const usCurrency = usCurrencyFormat.format(row.monto);
                var html=usCurrency;
                    return html;
                }
            },
            {"data": "fecha"},
            {"data": null,
                "render": function(data, type, row, meta){
                    var html = '<a href="'+base_url+'Prestamo/addprestamo/'+row.idprestamo+'" class="btn btn-round btn-success"><i class="fa fa-edit"></i></a>\
                                <button type="button" class="btn btn-round btn-danger nombre_p'+row.idprestamo+'" onclick="modal_eliminar('+row.idprestamo+')"><i class="fa fa-trash"></i></button>';

                    return html;
                }
            },
          ],
        order: [
            [0, "desc"]
          ],
    });
}
function modal_eliminar(id){
    $('#eliminar_modal').modal();
    $('#idprestamo_e').val(id);
}
function boton_eliminar(){
    $.ajax({
        type:'POST',
        url: base_url+'Prestamo/eliminar',
        data: {idp:$("#idprestamo_e").val()},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
            new PNotify({
                      title: 'Hecho!',
                       text: 'Eliminado Correctamente',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
        }
    });
    tabledata_detalles.ajax.reload();
    tabledata.ajax.reload();
}