var base_url = $('#base_url').val();
var tabledata;
$(document).ready(function(){
    tabledata = $('#data_tables').DataTable();
});
function loadtable(){
    tabledata = $('#data_tables').DataTable({
        fixedHeader: true,
        responsive: 0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "Nomina/getData",
            type: "post",
        },
        "columns": [
            {"data": "personalId"},
            {"data": "nombre"}, 
            {"data": "sucursal"},
            {"data": null,
                "render": function(data, type, row, meta ) {
                    const usCurrencyFormat = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD'});
                    const usCurrency = usCurrencyFormat.format(row.sueldo);
                    var html=usCurrency;
                    return html;
                }
            },
            {"data": null,
                "render": function(data, type, row, meta ) {
                    const usCurrencyFormat = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD'});
                    const usCurrency = usCurrencyFormat.format(row.prestamo);
                    var html=usCurrency;
                    var span='<span style="display:none" class="prestamo_'+row.personalId+'" data-prestamott="'+row.prestamo+'"></span>\
                              <span class="total_res_n_'+row.personalId+'">'+html+'</span>';
                    return span;
                }
            },
            {"data": null,
                "render": function(data, type, row, meta){
                    var aux_sueldo=row.sueldo;
                    var aux_prestamo=row.prestamo;
                    var resta=aux_sueldo-aux_prestamo;
                    const usCurrencyFormat = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD'});
                    const usCurrency = usCurrencyFormat.format(resta);
                    var html=usCurrency;
                    var span='<span style="display:none" class="total_res_'+row.personalId+'" data-resta_total="'+resta+'"></span>\
                              <span class="total_res_nt_'+row.personalId+'">'+html+'</span>';
                    return span;          
                }
            },
            {"data": null,
                "render": function(data, type, row, meta){
                    var aux_v=validar_nomina(row.personalId);
                    if(aux_v>=1){
                    var html ='<span style="color:black">Pagado</span>';    
                    }else{
                    var html = '<button class="btn btn-round btn-success per_'+row.personalId+'" data-sueldo="'+row.sueldo+'" onclick="pagar_nomina('+row.personalId+')">Pagar</button>';
                    }
                    return html;
                }
            },
          ],
        order: [
            [0, "desc"]
          ],
    });
}
function reload_table(){
    tabledata.destroy();
    loadtable();
}
function validar_nomina(id){
    var fecha = $('#fecha').val();
    var aux_v_n=0; 
    $.ajax({
        type:'POST',
        url: base_url+'Nomina/get_validar_nomina',
        data: {id:id,fecha:fecha},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
          array=$.parseJSON(data); 
          aux_v_n=array.aux_p;    
          if(aux_v_n>=1){
            const usCurrencyFormat = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD'});
            const usCurrency = usCurrencyFormat.format(array.prestamo);
            const usCurrencyFormata = new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD'});
            const usCurrencya = usCurrencyFormata.format(array.tt_pagar);
            $('.total_res_n_'+id).html(usCurrency);
            $('.total_res_nt_'+id).html(usCurrencya);
          }
        }
    });
    return aux_v_n;
    
}
/*
function prestamo(id){
    var fecha_inicio = $('#fechaini').val();
    var fecha_fin = $('#fechafin').val();
    var aux_pres=0;
    $.ajax({
        type:'POST',
        url: base_url+'Nomina/get_prestamo',
        data: {id:id,fechai:fecha_inicio,fechaf:fecha_fin},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
            aux_pres=data;     
        }
    });
    return aux_pres;
}
*/
/*
function validar_nomina(id){
    var fecha = $('#fecha').val();
    var aux_v_n=0; 
    $.ajax({
        type:'POST',
        url: base_url+'Nomina/get_validar_nomina',
        data: {id:id,fecha:fecha},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
           array=$.parseJSON(data); 
            aux_v_n=array.aux_p;     
        }
    });
    return aux_v_n;
    
}
*/
function pagar_nomina(id){
    var fecha=$('#fecha').val();
    var sueldo=$('.per_'+id).data('sueldo');
    var prestamo=$('.prestamo_'+id).data('prestamott');
    var total_s=$('.total_res_'+id).data('resta_total');
    $.ajax({
        type:'POST',
        url: base_url+'Nomina/registra_nomina',
        data:{id:id,
            fecha:fecha,
            sueldo:sueldo,
            prestamo:prestamo,
            total_s:total_s},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
            new PNotify({
                  title: 'Hecho!',
                   text: 'Guardado Correctamente',
                  type: 'success',
                  styling: 'bootstrap3'
              });
            reload_table();    
        }
    });
}