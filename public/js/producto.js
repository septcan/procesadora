var base_url=$('#base_url').val();
$(document).ready(function($) {
    $('.guardarform_pro').click(function(event) {
        var data = new FormData();
        if ($('#exampleInputFile')[0].files.length > 0) {
            var inputFileImage = document.getElementById('exampleInputFile');
            var file = inputFileImage.files[0]; 
            data.append('img',file);
        }
        //====================
            var DATAp  = [];
            var TABLAps   = $("#tableproductossucursal tbody > tr");
                TABLAps.each(function(){         
                    item = {};
                    item ['idsucursal'] = $(this).find("input[id*='idsucursal']").val();
                    item ['idproductosucursal'] = $(this).find("input[id*='idproductosucursal']").val();
                    item ['precioventa'] = $(this).find("input[id*='precioventa']").val();
                    item ['mayoreo'] = $(this).find("input[id*='mayoreo']").val();
                    item ['can_mayoreo'] = $(this).find("input[id*='can_mayoreo']").val();
                    item ['existencia'] = $(this).find("input[id*='existencia']").val();

                    DATAp.push(item);
                });
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATAp);
        data.append('stock',aInfo);
        //====================
        data.append('productoid',$('#productoid').val());
        data.append('codigo',$('#codigo').val());
        data.append('nombre',$('#nombre').val());
        data.append('descripcion',$('#descripcion').val());
        data.append('categoria',$('#categoria').val());
        data.append('preciocompra',$('#preciocompra').val());
        $.ajax({
            url:base_url+'Productos/add',
            type:'POST',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            success: function(data) {
                console.log(data);
                if (data==1) {
                    new PNotify({
                          title: 'Hecho!',
                          text: 'Guardado Correctamente',
                          type: 'success',
                          styling: 'bootstrap3'
                    });
                    $('.modalcategoriaadd').modal('hide');
                    setInterval(function(){ 
                        location.href=base_url+'Productos';
                    }, 3000);
                }else if(data==2){
                    new PNotify({
                            title: 'Error!',
                            text: 'Se a excedido el numero de productos permitidos',
                            type: 'error',
                            styling: 'bootstrap3'
                    });
                } 
            },
            error: function(jqXHR, textStatus, errorThrown) {
                var data = JSON.parse(jqXHR.responseText);
                console.log(data);
                if (data.ok=='true') {
                    $(".fileinput").fileinput("clear");
                }else{
                    toastr.error('Error', data.msg);
                }
            }
        });
    }); 
});